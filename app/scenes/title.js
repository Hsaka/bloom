import Utils from 'utils/utils';

export default class TitleScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'title'
    });
  }

  create() {
    Utils.load();

    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.clickSnd = this.sound.add('click');
    this.winSnd = this.sound.add('win');
    this.winSnd.setVolume(0.2);

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'titlebg');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.anims.create({
      key: 'fly',
      frames: this.anims.generateFrameNames('atlas1', {
        prefix: 'butterfly',
        start: 1,
        end: 8
      }),
      frameRate: 24,
      yoyo: true,
      repeat: -1
    });

    this.hsv = Phaser.Display.Color.HSVColorWheel();
    this.sprName = 'butterfly';
    this.numSprs = 1;
    this.dir = 'up';
    this.sprPool = this.add.group();
    for (var i = 0; i < 20; i++) {
      var spr = this.sprPool.create(0, 0, 'atlas1', this.sprName + '1');
      //spr.alpha = Phaser.Math.RND.realInRange(0.3, 1);
      spr._xspeed = Phaser.Math.RND.realInRange(-0.3, 0.3);
      spr._yspeed = Phaser.Math.RND.realInRange(1.2, 1.4);
      spr._curYSpeed = 0;
      spr.setFrame(
        this.sprName + Phaser.Math.RND.integerInRange(1, this.numSprs)
      );
      spr.setScale(Phaser.Math.RND.realInRange(0.3, 0.5));
      spr.x = Phaser.Math.RND.integerInRange(0, Utils.GlobalSettings.width);

      if (this.dir === 'up') {
        spr.y = Phaser.Math.RND.integerInRange(
          Utils.GlobalSettings.height + 100,
          Utils.GlobalSettings.height * 2
        );
      } else {
        spr.y = Phaser.Math.RND.integerInRange(
          -100,
          -Utils.GlobalSettings.height
        );
      }

      spr.anims.load('fly');
      spr.anims.play('fly', true, Phaser.Math.RND.integerInRange(1, 8));
      spr.setTintFill(this.hsv[Phaser.Math.RND.integerInRange(0, 359)].color);
    }

    this.anims.create({
      key: 'bloom',
      frames: this.anims.generateFrameNames('atlas1', {
        prefix: 'bloom_',
        start: 0,
        end: 20
      }),
      frameRate: 16
    });

    this.title = this.add.sprite(
      Utils.GlobalSettings.width / 2,
      200,
      'atlas1',
      'bloom_0'
    );
    this.title.setScale(5);
    this.title.anims.load('bloom');
    this.title.play('bloom');

    this.tweens.add({
      targets: this.title,
      scale: 2,
      ease: 'Back.easeOut',
      duration: 1000
    });

    this.tweens.add({
      targets: this.title,
      scale: 1.8,
      ease: 'Sine.easeInOut',
      duration: 1000,
      delay: 2000,
      yoyo: true,
      repeat: -1
    });

    this.playBtn = this.add.image(
      Utils.GlobalSettings.width / 2 - 100,
      Utils.GlobalSettings.height - 250,
      'atlas1',
      'play0'
    );
    this.playBtn.setInteractive();
    this.playBtn.setScrollFactor(0);

    this.playBtn.on('pointerover', event => {
      this.playBtn.setFrame('play1');
    });

    this.playBtn.on('pointerout', event => {
      this.playBtn.setFrame('play0');
    });
    this.playBtn.on('pointerdown', () => {
      Utils.SavedSettings.level = 1;
      Utils.save();

      if (!Utils.SavedSettings.muted) {
        this.winSnd.play();
      }

      this.cameras.main.fade(
        1000,
        Phaser.Math.Between(50, 255),
        Phaser.Math.Between(50, 255),
        Phaser.Math.Between(50, 255)
      );

      this.cameras.main.on(
        'camerafadeoutcomplete',
        function() {
          this.scene.start('main');
        },
        this
      );
    });

    this.playText = this.add.bitmapText(
      this.playBtn.x,
      this.playBtn.y + 40,
      'modak',
      'New Game',
      30
    );
    this.playText.x = this.playBtn.x - this.playText.width / 2;

    this.continueBtn = this.add.image(
      Utils.GlobalSettings.width / 2 + 100,
      Utils.GlobalSettings.height - 250,
      'atlas1',
      'play0'
    );
    this.continueBtn.setInteractive();
    this.continueBtn.setScrollFactor(0);

    this.continueBtn.on('pointerover', event => {
      this.continueBtn.setFrame('play1');
    });

    this.continueBtn.on('pointerout', event => {
      this.continueBtn.setFrame('play0');
    });
    this.continueBtn.on('pointerdown', () => {
      if (!Utils.SavedSettings.muted) {
        this.winSnd.play();
      }

      this.cameras.main.fade(
        1000,
        Phaser.Math.Between(50, 255),
        Phaser.Math.Between(50, 255),
        Phaser.Math.Between(50, 255)
      );

      this.cameras.main.on(
        'camerafadeoutcomplete',
        function() {
          this.scene.start('main');
        },
        this
      );
    });

    this.continueText = this.add.bitmapText(
      this.continueBtn.x,
      this.continueBtn.y + 40,
      'modak',
      'Continue',
      30
    );
    this.continueText.x = this.continueBtn.x - this.continueText.width / 2;

    if (Utils.SavedSettings.level === 1) {
      this.continueBtn.visible = false;
      this.continueText.visible = false;
      this.playBtn.x = Utils.GlobalSettings.width / 2;
      this.playText.x = this.playBtn.x - this.playText.width / 2;
    }

    this.creditIndex = 0;
    this.creditText = [
      'Code: Hsaka',
      'Music: Tom Peter',
      'Artwork: bevouliin',
      'Artwork: peony',
      'Artwork: William.Thompsonj',
      'Artwork: dontmind8.blogspot.com'
    ];

    this.credits = this.add.bitmapText(
      10,
      Utils.GlobalSettings.height,
      'modak',
      this.creditText[0],
      50
    );
    this.credits.alpha = 0;

    this.tweens.add({
      targets: this.credits,
      alpha: 1,
      y: Utils.GlobalSettings.height - 50,
      ease: 'Sine.easeOut',
      duration: 2500,
      delay: 100,
      yoyo: true,
      loop: -1,
      onLoop: () => {
        this.creditIndex++;
        if (this.creditIndex >= this.creditText.length) {
          this.creditIndex = 0;
        }
        this.credits.setText(this.creditText[this.creditIndex]);
      }
    });

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI', { showRestart: false, showHome: false });
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI', { showRestart: false, showHome: false });
      this.scene.bringToTop('gameUI');
    }
    this.registry.set('showRestart', false);
    this.registry.set('showHome', false);

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    }
  }

  updateSprites(sprite) {
    if (sprite) {
      if (
        (this.dir === 'down' && sprite.y > Utils.GlobalSettings.height + 50) ||
        (this.dir === 'up' && sprite.y < -50)
      ) {
        sprite.x = Phaser.Math.RND.integerInRange(
          0,
          Utils.GlobalSettings.width
        );

        if (this.dir === 'up') {
          sprite.y = Phaser.Math.RND.integerInRange(
            Utils.GlobalSettings.height + 100,
            Utils.GlobalSettings.height * 2
          );
        } else {
          sprite.y = Phaser.Math.RND.integerInRange(
            -100,
            -Utils.GlobalSettings.height
          );
        }

        sprite.setFrame(
          this.sprName + Phaser.Math.RND.integerInRange(1, this.numSprs)
        );
        //sprite.alpha = Phaser.Math.RND.realInRange(0.3, 1);
        sprite._xspeed = Phaser.Math.RND.realInRange(-0.3, 0.3);
        sprite._yspeed = Phaser.Math.RND.realInRange(1.2, 1.4);
        sprite.setScale(Phaser.Math.RND.realInRange(0.3, 0.5));
        sprite.setTintFill(
          this.hsv[Phaser.Math.RND.integerInRange(0, 359)].color
        );
      } else {
        sprite.x += sprite._xspeed;
        // if (sprite.alpha > 0) {
        //   sprite.alpha -= 0.0002;
        // }

        // sprite.angle += sprite._xspeed;
        // if (sprite.angle > 360) {
        //   sprite.angle = 0;
        // }

        if (sprite._curYSpeed < sprite._yspeed) {
          sprite._curYSpeed += 0.01;
        } else {
          sprite._curYSpeed = sprite._yspeed;
        }

        if (this.dir === 'up') {
          sprite.y -= sprite._curYSpeed;
        } else {
          sprite.y += sprite._curYSpeed;
        }
      }
    }
  }

  update() {
    if (this.sprPool) {
      this.sprPool.children.each(this.updateSprites.bind(this));
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    this.hsv = null;

    if (this.sprPool) {
      this.sprPool.destroy();
      this.sprPool = null;
    }

    if (this.title) {
      this.title.destroy();
      this.title = null;
    }

    if (this.playBtn) {
      this.playBtn.destroy();
      this.playBtn = null;
    }

    this.creditText = null;

    if (this.credits) {
      this.credits.destroy();
      this.credits = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill title');
  }
}
