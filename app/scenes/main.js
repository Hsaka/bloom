import Utils from 'utils/utils';

export default class MainScene extends Phaser.Scene {
  constructor() {
    super('main');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    Utils.load();

    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.drawSnd = this.sound.add('draw');
    this.drawSnd.setLoop(true);
    this.drawSnd.setVolume(0.1);
    this.collectSnd = this.sound.add('collect');
    // this.collectSnd.setVolume(0.1);
    this.winSnd = this.sound.add('win');
    this.winSnd.setVolume(0.2);
    this.deadSnd = this.sound.add('dead');
    this.deadSnd.setVolume(0.2);
    this.endSnd = this.sound.add('end');
    this.endSnd.setVolume(0.2);

    this.hsv = Phaser.Display.Color.HSVColorWheel();
    var i = 0;

    this.lastPx = -1;
    this.lastPy = -1;
    this.startX = -1;
    this.isDrawing = false;
    this.canDraw = true;
    this.points = [];
    this.drawTime = 0;
    this.maxLevels = 15;
    this.level = Utils.SavedSettings.level;
    this.numSeeds = 0;
    this.seedsBloomed = 0;
    this.maxTime = 5000;
    this.gameOver = false;
    this.levelCompleted = false;

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'bg');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.bg2 = this.add.image(0, -Utils.GlobalSettings.height, 'bg');
    this.bg2.setOrigin(0, 0);
    this.bg2.setVisible(false);
    this.screenGroup.add(this.bg2);

    this.levelTxt = this.add.dynamicBitmapText(
      Utils.GlobalSettings.width / 2,
      -200,
      'modak',
      'Level: 1',
      80
    );
    this.levelTxt.setScrollFactor(0);
    this.levelTxtOffset = 10;
    this.levelTxt.setDisplayCallback(data => {
      if (this.levelTxt.visible) {
        data.x = Phaser.Math.Between(
          data.x - (this.levelTxtOffset + 0.1),
          data.x + (this.levelTxtOffset + 0.1)
        );
        data.y = Phaser.Math.Between(
          data.y - (this.levelTxtOffset + 0.2),
          data.y + (this.levelTxtOffset + 0.2)
        );
        this.levelTxtOffset -= 0.005;
        if (this.levelTxtOffset < 1) {
          this.levelTxtOffset = 1;
        }
        return data;
      }
    });
    this.levelTxt.x = Utils.GlobalSettings.width / 2 - this.levelTxt.width / 2;
    this.screenGroup.add(this.levelTxt);

    this.pad = this.add.renderTexture(
      0,
      Utils.GlobalSettings.height - 400,
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.screenGroup.add(this.pad);

    this.tutorialImage = this.add.image(
      this.pad.x,
      this.pad.y,
      'atlas1',
      'tutorial1'
    );
    this.tutorialImage.setOrigin(0);
    this.tutorialImage.visible = false;
    this.tutorialHand = this.add.image(0, 0, 'atlas1', 'hand');
    this.tutorialHand.visible = false;
    this.screenGroup.add(this.tutorialImage);
    this.screenGroup.add(this.tutorialHand);

    this.graphics = this.make.graphics();
    this.pad.drawFrame('atlas1', 'pad');

    this.timerText = this.add.bitmapText(
      20,
      Utils.GlobalSettings.height - 50,
      'modak',
      '',
      60
    );
    this.timerText.visible = false;
    this.screenGroup.add(this.timerText);

    if (!Utils.mobilecheck()) {
      this.particles = this.add.particles('atlas1');
      this.screenGroup.add(this.particles);

      this.emitter = this.particles.createEmitter({
        frame: { frames: ['yellow', 'green', 'blue'], cycle: true },
        scale: { start: 0.1, end: 0 },
        blendMode: 'ADD',
        lifespan: 1000,
        delay: 1000
      });
    }

    this.player = this.add.follower(
      null,
      Utils.GlobalSettings.width / 2,
      this.pad.y - 100,
      'atlas1',
      'arrow'
    );
    this.screenGroup.add(this.player);
    // this.player.setScale(1.5);

    this.anims.create({
      key: 'rest',
      frames: this.anims.generateFrameNames('atlas1', {
        prefix: 'butterfly',
        start: 1,
        end: 8
      }),
      frameRate: 6,
      yoyo: true,
      repeat: -1
    });

    this.anims.create({
      key: 'fly',
      frames: this.anims.generateFrameNames('atlas1', {
        prefix: 'butterfly',
        start: 1,
        end: 8
      }),
      frameRate: 24,
      yoyo: true,
      repeat: -1
    });

    this.player.anims.load('rest');
    this.player.anims.load('fly');
    this.player.anims.play('rest');

    this.playerGroup = this.physics.add.group({ allowGravity: false });
    this.playerGroup.add(this.player);
    this.player.body.setSize(24, 32, true);

    this.bushGroup = this.add.group();

    if (!Utils.mobilecheck()) {
      this.emitter.startFollow(this.player);
    }

    this.anims.create({
      key: 'spin',
      frames: this.anims.generateFrameNames('atlas1', {
        prefix: 'seed0',
        start: 0,
        end: 11
      }),
      frameRate: 8,
      repeat: -1
    });

    this.collectibleGroup = this.physics.add.group();
    for (var i = 0; i < 10; i++) {
      var spr = this.collectibleGroup.create(
        Utils.GlobalSettings.width / 2,
        100,
        'atlas1',
        'seed00'
      );
      spr.anims.load('spin');
      spr.setImmovable();
      spr.disableBody(true, true);
      this.collectibleGroup.killAndHide(spr);
    }

    this.physics.add.overlap(
      this.playerGroup,
      this.collectibleGroup,
      this.hitCollectible,
      null,
      this
    );

    this.anims.create({
      key: 'crawl',
      frames: this.anims.generateFrameNames('atlas1', {
        prefix: 'spider_',
        start: 0,
        end: 4
      }),
      frameRate: 16,
      repeat: -1
    });

    this.anims.create({
      key: 'bite',
      frames: this.anims.generateFrameNames('atlas1', {
        prefix: 'plant_',
        start: 1,
        end: 2
      }),
      frameRate: 16,
      repeat: -1
    });

    // this.obstacleGroup = this.physics.add.group();
    this.obstacleGroup = [];

    this.obstaclePathGraphics = this.add.graphics();
    this.obstaclePathGraphics.lineStyle(1, 0xffffff, 1);

    for (var i = 0; i < 10; i++) {
      var spr = this.add.follower(null, 0, 0, 'atlas1', 'spider_0');

      this.obstacleGroup.push(spr);
      this.physics.world.enableBody(spr);

      spr.anims.load('crawl');
      spr.anims.load('bite');
      spr.body.setImmovable(true);

      spr.body.setEnable(false);
      spr.visible = false;
      // this.obstacleGroup.killAndHide(spr);
    }

    this.physics.add.overlap(
      this.playerGroup,
      this.obstacleGroup,
      this.hitObstacle,
      null,
      this
    );

    this.drawTimer = undefined;

    this.input.on(
      'pointerup',
      pointer => {
        this.stopDraw();
      },
      this
    );

    this.input.on(
      'pointermove',
      pointer => {
        if (!this.gameOver && !this.levelCompleted && pointer.isDown) {
          var px = pointer.x;
          var py = pointer.y;

          if (py >= this.pad.y && this.canDraw) {
            if (this.tutorialHand.visible) {
              this.tutorialHand.visible = false;
            }

            if (!Utils.SavedSettings.muted) {
              if (!this.drawSnd.isPlaying) {
                this.drawSnd.play();
              }
            }

            this.isDrawing = true;

            this.drawTime++;

            if (this.lastPx !== -1 && this.lastPy !== -1) {
              this.graphics.lineStyle(8, this.hsv[i].color, 1);
              this.graphics.beginPath();
              this.graphics.moveTo(this.lastPx, this.lastPy - this.pad.y);
              this.graphics.lineTo(px, py - this.pad.y);
              this.graphics.closePath();
              this.graphics.strokePath();
              this.pad.draw(this.graphics);
            }

            this.pad.drawFrame(
              'atlas1',
              'brush',
              px - 8,
              py - 8 - this.pad.y,
              1,
              this.hsv[i].color
            );

            if (this.drawTime % 2 === 0) {
              this.points.push(px);
              this.points.push(py);
            }

            if (this.startX === -1) {
              this.startX = px;
              //this.player.x = this.startX;
              this.tweens.add({
                targets: this.player,
                x: this.startX,
                ease: 'Power1',
                duration: 100
              });

              this.drawTimer = this.time.delayedCall(
                this.maxTime,
                this.drawTimerElapsed,
                [],
                this
              );

              this.timerText.visible = true;
              this.timerText.setText('' + this.maxTime / 1000);
            }

            this.lastPx = px;
            this.lastPy = py;

            i++;

            if (i === 360) {
              i = 0;
            }
          }
        }
      },
      this
    );

    this.gameOverTxt = this.add.dynamicBitmapText(
      Utils.GlobalSettings.width / 2,
      this.pad.y + 100,
      'modak',
      'Game Over!',
      80
    );
    this.gameOverTxt.setVisible(false);
    this.gameOverTxt.setAlpha(0);
    this.gameOverTxt.setOrigin(0);
    this.gameOverTxt.setScrollFactor(0);
    this.gameOverTxtOffset = 10;
    this.gameOverTxt.setDisplayCallback(data => {
      if (this.gameOverTxt.visible) {
        data.x = Phaser.Math.Between(
          data.x - (this.gameOverTxtOffset + 0.1),
          data.x + (this.gameOverTxtOffset + 0.1)
        );
        data.y = Phaser.Math.Between(
          data.y - (this.gameOverTxtOffset + 0.2),
          data.y + (this.gameOverTxtOffset + 0.2)
        );
        this.gameOverTxtOffset -= 0.005;
        if (this.gameOverTxtOffset < 1) {
          this.gameOverTxtOffset = 1;
        }
        return data;
      }
    });
    this.gameOverTxt.x =
      Utils.GlobalSettings.width / 2 - this.gameOverTxt.width / 2;

    this.restartBtn = this.add.image(
      Utils.GlobalSettings.width / 2,
      this.gameOverTxt.y + 150,
      'atlas1',
      'restart0'
    );
    this.restartBtn.setInteractive();
    this.restartBtn.setScrollFactor(0);
    this.restartBtn.setVisible(false);
    this.restartBtn.setAlpha(0);

    this.restartBtn.on('pointerover', event => {
      this.restartBtn.setFrame('restart1');
    });

    this.restartBtn.on('pointerout', event => {
      this.restartBtn.setFrame('restart0');
    });
    this.restartBtn.on('pointerdown', () => {
      this.restartBtn.visible = false;

      // if (!Utils.SavedSettings.muted) {
      //   var bgmTween = this.tweens.add({
      //     targets: this.bgmSnd,
      //     volume: 0,
      //     ease: 'Linear',
      //     duration: 1000
      //   });
      // }

      this.cameras.main.fade(
        1000,
        Phaser.Math.Between(50, 255),
        Phaser.Math.Between(50, 255),
        Phaser.Math.Between(50, 255)
      );
    });

    this.cameras.main.on(
      'camerafadeoutcomplete',
      function() {
        this.scene.restart();
      },
      this
    );

    this.generateLevel();

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI', { showRestart: true });
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI', { showRestart: true });
      this.scene.bringToTop('gameUI');
    }
    this.registry.set('showRestart', true);
    this.registry.set('showHome', true);

    this.showTutorial();
  }

  drawTimerElapsed() {
    this.timerText.setText('0');
    this.stopDraw();
  }

  stopDraw() {
    if (!Utils.SavedSettings.muted) {
      this.drawSnd.stop();
    }

    if (this.drawTimer) {
      this.drawTimer.remove();
      this.drawTimer = undefined;
    }

    if (this.canDraw) {
      this.lastPx = -1;
      this.lastPy = -1;
      this.startX = -1;
      this.drawTime = 0;
      this.isDrawing = false;
      this.canDraw = false;
      this.timerText.visible = false;

      if (this.points.length > 10) {
        this.movePlayer();
      } else {
        this.pad.clear();
        this.graphics.clear();
        this.pad.drawFrame('atlas1', 'pad');
        this.points = [];
        this.canDraw = true;
      }
    }
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  showTutorial() {
    this.tutorialImage.visible = false;
    this.tutorialHand.visible = false;
    this.tutorialImage.alpha = 0;
    this.tutorialHand.alpha = 0;
    switch (this.level) {
    case 1:
      this.tutorialImage.visible = true;
      this.tutorialHand.visible = true;

      this.tweens.add({
        targets: [this.tutorialImage],
        alpha: 1,
        ease: 'Sine.easeOut',
        duration: 1000,
        delay: 1000
      });

      this.tutorialHand.x = Utils.GlobalSettings.width / 2 + 15;
      this.tutorialHand.y = Utils.GlobalSettings.height - 50;
      this.tutorialHand.angle = -30;

      this.tweens.add({
        targets: [this.tutorialHand],
        alpha: 1,
        ease: 'Sine.easeOut',
        duration: 1000,
        delay: 1500
      });

      this.tweens.add({
        targets: [this.tutorialHand],
        y: Utils.GlobalSettings.height - 300,
        ease: 'Sine.easeOut',
        duration: 2000,
        delay: 2000,
        repeat: -1
      });
      break;

    case 2:
      this.tutorialImage.visible = true;
      this.tutorialImage.setFrame('tutorial2');

      this.tweens.add({
        targets: [this.tutorialImage],
        alpha: 1,
        ease: 'Sine.easeOut',
        duration: 1000,
        delay: 1000
      });
      break;

    case 3:
      this.tutorialImage.visible = true;
      this.tutorialImage.setFrame('tutorial3');

      this.tweens.add({
        targets: [this.tutorialImage],
        alpha: 1,
        ease: 'Sine.easeOut',
        duration: 1000,
        delay: 1000
      });
      break;
    }
  }

  hitCollectible(playerGroup, collectible) {
    if (!this.gameOver && !this.levelCompleted) {
      this.generateBush(collectible.x, collectible.y);
      collectible.disableBody(true, true);

      this.seedsBloomed++;
    }
  }

  hitObstacle(playerGroup, obstacle) {
    obstacle.body.setEnable(false);
    this.doGameOver();
  }

  doGameOver() {
    if (!Utils.SavedSettings.muted) {
      this.deadSnd.play();
    }

    this.gameOver = true;
    this.player.stopFollow();
    this.player.anims.stop();

    this.player.setScale(1);
    this.player.setFrame('butterfly1');
    this.player.setAlpha(1);
    this.player.setAngle(0);
    this.tweens.add({
      targets: this.player,
      angle: 720 * 2,
      scaleX: 5,
      scaleY: 5,
      alpha: 0,
      ease: 'Sine.easeOut',
      duration: 2000
    });

    this.gameOverTxt.setVisible(true);
    var textTween = this.tweens.add({
      targets: this.gameOverTxt,
      alpha: 1,
      ease: 'Linear',
      duration: 1000,
      onComplete: () => {}
    });

    this.restartBtn.setVisible(true);
    var restartTween1 = this.tweens.add({
      targets: this.restartBtn,
      alpha: 1,
      ease: 'Linear',
      duration: 1000,
      delay: 1500
    });

    var restartTween2 = this.tweens.add({
      targets: this.restartBtn,
      angle: 360,
      ease: 'Linear',
      duration: 3000,
      loop: -1
    });

    // if (!Utils.SavedSettings.muted) {
    //   var bgmTween = this.tweens.add({
    //     targets: this.bgmSnd,
    //     volume: 0.6,
    //     ease: 'Linear',
    //     duration: 5000
    //   });
    // }
  }

  generateLevel() {
    switch (this.level) {
    case 1:
      var collectible = this.getCollectible(
        Utils.GlobalSettings.width / 2,
        200
      );

      this.numSeeds = 1;
      break;

    case 2:
      var collectible1 = this.getCollectible(
        Utils.GlobalSettings.width / 2,
        300
      );

      var obstacles = this.generateObstacles(
        collectible1.x,
        collectible1.y - 100,
        3
      );
      obstacles[0].body.setSize(40, 60, true);
      obstacles[0].anims.play('bite');

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(collectible1.x - 200, collectible1.y - 20);
      obstacles[1].anims.play('bite');

      obstacles[2].body.setSize(40, 60, true);
      obstacles[2].setPosition(collectible1.x + 200, collectible1.y - 20);
      obstacles[2].anims.play('bite');

      var collectible2 = this.getCollectible(
        obstacles[0].x - 100,
        obstacles[0].y
      );

      this.numSeeds = 2;
      break;

    case 3:
      var obstacles = this.generateObstacles(150, 360, 3);
      obstacles[0].body.setSize(40, 60, true);
      obstacles[0].anims.play('bite');

      var collectible1 = this.getCollectible(
        obstacles[0].x + 100,
        obstacles[0].y
      );

      var collectible2 = this.getCollectible(
        obstacles[0].x + 300,
        obstacles[0].y
      );

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(collectible1.x + 100, collectible1.y);
      obstacles[1].anims.play('bite');

      obstacles[2].body.setSize(40, 60, true);
      obstacles[2].setPosition(collectible1.x + 300, collectible1.y);
      obstacles[2].anims.play('bite');

      this.numSeeds = 2;
      break;

    case 4:
      var obstacles = this.generateObstacles(
        Utils.GlobalSettings.width / 2,
        250,
        1
      );
      obstacles[0].body.setSize(40, 60, true);
      obstacles[0].anims.play('bite');

      this.getCollectible(obstacles[0].x - 100, obstacles[0].y);

      this.getCollectible(obstacles[0].x + 100, obstacles[0].y);

      this.getCollectible(obstacles[0].x, obstacles[0].y - 100);

      this.getCollectible(obstacles[0].x, obstacles[0].y + 100);

      this.numSeeds = 4;
      break;

    case 5:
      var obstacles = this.generateObstacles(100, this.player.y, 4);
      obstacles[0].body.setSize(40, 60, true);
      obstacles[0].anims.play('bite');

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(obstacles[0].x + 125, obstacles[0].y);
      obstacles[1].anims.play('bite');

      obstacles[2].body.setSize(40, 60, true);
      obstacles[2].setPosition(
        Utils.GlobalSettings.width - 100,
        obstacles[0].y
      );
      obstacles[2].anims.play('bite');

      obstacles[3].body.setSize(40, 60, true);
      obstacles[3].setPosition(obstacles[2].x - 125, obstacles[0].y);
      obstacles[3].anims.play('bite');

      this.getCollectible(obstacles[0].x, obstacles[0].y - 100);
      this.getCollectible(obstacles[2].x, obstacles[2].y - 100);
      this.getCollectible(obstacles[1].x, obstacles[1].y - 200);
      this.getCollectible(obstacles[3].x, obstacles[3].y - 200);
      this.getCollectible(this.player.x, this.player.y - 300);

      this.numSeeds = 5;
      break;

    case 6:
      var obstacles = this.generateObstacles(100, this.player.y - 200, 3);
      obstacles[0].body.setSize(40, 60, true);
      obstacles[0].anims.play('bite');

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(this.player.x, this.player.y - 100);
      obstacles[1].anims.play('bite');

      obstacles[2].body.setSize(40, 60, true);
      obstacles[2].setPosition(
        Utils.GlobalSettings.width - 100,
        obstacles[0].y
      );
      obstacles[2].anims.play('bite');

      this.getCollectible(obstacles[0].x, obstacles[0].y - 100);
      this.getCollectible(obstacles[2].x, obstacles[2].y - 100);
      this.getCollectible(obstacles[1].x, obstacles[1].y - 200);
      this.getCollectible(obstacles[0].x, obstacles[0].y + 100);
      this.getCollectible(obstacles[2].x, obstacles[2].y + 100);

      this.numSeeds = 5;
      break;

    case 7:
      this.player.y = 100;
      var obstacles = this.generateObstacles(
        this.player.x,
        this.player.y + 100,
        2
      );
      obstacles[0].body.setSize(40, 60, true);
      obstacles[0].anims.play('bite');

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(obstacles[0].x, obstacles[0].y + 200);
      obstacles[1].anims.play('bite');

      this.getCollectible(obstacles[0].x, obstacles[0].y + 100);
      this.getCollectible(obstacles[1].x, obstacles[1].y + 100);

      this.numSeeds = 2;
      break;

    case 8:
      this.player.x = 50;
      this.player.y = 275;
      var obstacles = this.generateObstacles(
        this.player.x + 100,
        this.player.y,
        2
      );
      obstacles[0].body.setSize(40, 60, true);
      obstacles[0].anims.play('bite');

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(obstacles[0].x + 300, obstacles[0].y);
      obstacles[1].anims.play('bite');

      this.getCollectible(obstacles[0].x + 100, obstacles[0].y - 200);
      this.getCollectible(obstacles[0].x + 100, obstacles[0].y + 200);
      this.getCollectible(obstacles[1].x + 100, obstacles[1].y - 200);
      this.getCollectible(obstacles[1].x + 100, obstacles[1].y + 200);
      this.getCollectible(obstacles[0].x + 200, obstacles[0].y);

      this.numSeeds = 5;
      break;

    case 9:
      this.player.x = Utils.GlobalSettings.width / 2;
      this.player.y = 275;
      var obstacles = this.generateObstacles(
        this.player.x - 150,
        this.player.y - 200,
        5
      );
      obstacles[0].body.setSize(40, 60, true);
      obstacles[0].anims.play('bite');

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(this.player.x + 150, obstacles[0].y);
      obstacles[1].anims.play('bite');

      obstacles[2].body.setSize(40, 60, true);
      obstacles[2].setPosition(this.player.x - 100, this.player.y);
      obstacles[2].anims.play('bite');

      obstacles[3].body.setSize(40, 60, true);
      obstacles[3].setPosition(this.player.x + 100, this.player.y);
      obstacles[3].anims.play('bite');

      obstacles[4].body.setSize(40, 60, true);
      obstacles[4].setPosition(this.player.x, this.player.y + 100);
      obstacles[4].anims.play('bite');

      this.getCollectible(this.player.x, obstacles[0].y);
      this.getCollectible(obstacles[2].x - 100, obstacles[2].y);
      this.getCollectible(obstacles[3].x + 100, obstacles[3].y);

      this.numSeeds = 3;
      break;

    case 10:
      var obstacles = this.generateObstacles(0, 0, 3);
      obstacles[0].anims.play('crawl');

      var path = new Phaser.Curves.Path(200, 275).lineTo(
        Utils.GlobalSettings.width - 200,
        275
      );

      path.draw(this.obstaclePathGraphics, 16);

      obstacles[0].setPath(path);
      obstacles[0].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: true,
        repeat: -1,
        rotateToPath: false,
        verticalAdjust: true
      });

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(this.player.x + 180, obstacles[0].y);
      obstacles[1].anims.play('bite');

      obstacles[2].flipX = true;
      obstacles[2].body.setSize(40, 60, true);
      obstacles[2].setPosition(this.player.x - 180, obstacles[0].y);
      obstacles[2].anims.play('bite');

      this.getCollectible(this.player.x, obstacles[0].y);

      this.numSeeds = 1;
      break;

    case 11:
      var obstacles = this.generateObstacles(0, 0, 3);
      obstacles[0].anims.play('crawl');

      var path1 = new Phaser.Curves.Path(200, 230).circleTo(100);
      path1.draw(this.obstaclePathGraphics, 16);
      obstacles[0].setPath(path1);
      obstacles[0].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: false,
        repeat: -1,
        rotateToPath: true,
        verticalAdjust: true
      });

      var path2 = new Phaser.Curves.Path(
        Utils.GlobalSettings.width / 2 + 100,
        230
      ).circleTo(100);
      path2.draw(this.obstaclePathGraphics, 16);
      obstacles[1].setPath(path2);
      obstacles[1].anims.play('crawl');
      obstacles[1].startFollow({
        positionOnPath: true,
        duration: 2500,
        yoyo: false,
        repeat: -1,
        rotateToPath: true,
        verticalAdjust: true
      });

      var path3 = new Phaser.Curves.Path(
        Utils.GlobalSettings.width,
        230
      ).circleTo(100);
      path3.draw(this.obstaclePathGraphics, 16);
      obstacles[2].setPath(path3);
      obstacles[2].anims.play('crawl');
      obstacles[2].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: false,
        repeat: -1,
        rotateToPath: true,
        verticalAdjust: true
      });

      this.getCollectible(this.player.x, obstacles[0].y);
      this.getCollectible(100, obstacles[0].y);
      this.getCollectible(Utils.GlobalSettings.width - 100, obstacles[0].y);

      this.numSeeds = 3;
      break;

    case 12:
      var obstacles = this.generateObstacles(0, 0, 6);
      obstacles[0].anims.play('crawl');

      var path1 = new Phaser.Curves.Path(150, this.player.y - 100).lineTo(
        Utils.GlobalSettings.width - 150,
        this.player.y - 100
      );

      path1.draw(this.obstaclePathGraphics, 16);

      obstacles[0].setPath(path1);
      obstacles[0].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: true,
        repeat: -1,
        rotateToPath: false,
        verticalAdjust: true
      });

      var path2 = new Phaser.Curves.Path(150, this.player.y - 300).lineTo(
        Utils.GlobalSettings.width - 150,
        this.player.y - 300
      );

      path2.draw(this.obstaclePathGraphics, 16);

      obstacles[1].setPath(path2);
      obstacles[1].anims.play('crawl');
      obstacles[1].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: true,
        repeat: -1,
        rotateToPath: false,
        verticalAdjust: true,
        startAt: 1.0
      });

      var path3 = new Phaser.Curves.Path(150, this.player.y - 100).lineTo(
        150,
        this.player.y - 300
      );

      path3.draw(this.obstaclePathGraphics, 16);

      obstacles[2].setPath(path3);
      obstacles[2].anims.play('crawl');
      obstacles[2].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: true,
        repeat: -1,
        rotateToPath: false,
        verticalAdjust: true,
        startAt: 1.0
      });

      var path4 = new Phaser.Curves.Path(
        Utils.GlobalSettings.width - 150,
        this.player.y - 100
      ).lineTo(Utils.GlobalSettings.width - 150, this.player.y - 300);

      path4.draw(this.obstaclePathGraphics, 16);

      obstacles[3].setPath(path4);
      obstacles[3].anims.play('crawl');
      obstacles[3].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: true,
        repeat: -1,
        rotateToPath: false,
        verticalAdjust: true
      });

      obstacles[4].flipX = true;
      obstacles[4].body.setSize(40, 60, true);
      obstacles[4].setPosition(75, this.player.y - 25);
      obstacles[4].anims.play('bite');

      obstacles[5].body.setSize(40, 60, true);
      obstacles[5].setPosition(
        Utils.GlobalSettings.width - 75,
        this.player.y - 25
      );
      obstacles[5].anims.play('bite');

      this.getCollectible(this.player.x, this.player.y - 200);
      this.getCollectible(75, this.player.y - 200);
      this.getCollectible(
        Utils.GlobalSettings.width - 75,
        this.player.y - 200
      );
      this.getCollectible(this.player.x, this.player.y - 375);

      this.numSeeds = 4;
      break;

    case 13:
      this.player.x = 50;
      this.player.y = 50;
      var obstacles = this.generateObstacles(0, 0, 4);
      obstacles[0].anims.play('crawl');

      var path = new Phaser.Curves.Path(100, 100).lineTo(
        Utils.GlobalSettings.width - 100,
        this.pad.y - 100
      );

      path.draw(this.obstaclePathGraphics, 16);

      obstacles[0].setPath(path);
      obstacles[0].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: true,
        repeat: -1,
        rotateToPath: true,
        verticalAdjust: true
      });

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(this.player.x + 140, this.player.y + 25);
      obstacles[1].anims.play('bite');

      obstacles[2].body.setSize(40, 60, true);
      obstacles[2].setPosition(this.player.x + 140, this.player.y + 225);
      obstacles[2].anims.play('bite');

      obstacles[3].body.setSize(40, 60, true);
      obstacles[3].setPosition(this.player.x + 370, this.player.y + 215);
      obstacles[3].anims.play('bite');

      this.getCollectible(this.player.x + 140, this.player.y + 125);
      this.getCollectible(this.player.x + 260, this.player.y + 225);
      this.getCollectible(this.player.x + 380, this.player.y + 325);

      this.numSeeds = 3;
      break;

    case 14:
      this.player.y = 275;
      var obstacles = this.generateObstacles(0, 0, 5);
      obstacles[0].anims.play('crawl');

      var path = new Phaser.Curves.Path(
        this.player.x + 125,
        this.player.y
      ).circleTo(125);
      path.draw(this.obstaclePathGraphics, 16);
      obstacles[0].setPath(path);
      obstacles[0].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: false,
        repeat: -1,
        rotateToPath: true,
        verticalAdjust: true
      });

      obstacles[1].body.setSize(40, 60, true);
      obstacles[1].setPosition(this.player.x - 200, this.player.y - 100);
      obstacles[1].anims.play('bite');

      obstacles[2].body.setSize(40, 60, true);
      obstacles[2].setPosition(this.player.x - 200, this.player.y + 100);
      obstacles[2].anims.play('bite');

      obstacles[3].body.setSize(40, 60, true);
      obstacles[3].setPosition(this.player.x + 200, this.player.y - 100);
      obstacles[3].anims.play('bite');

      obstacles[4].body.setSize(40, 60, true);
      obstacles[4].setPosition(this.player.x + 200, this.player.y + 100);
      obstacles[4].anims.play('bite');

      this.getCollectible(obstacles[1].x + 130, obstacles[1].y);
      this.getCollectible(obstacles[1].x, obstacles[1].y - 100);
      this.getCollectible(this.player.x - 280, this.player.y);
      this.getCollectible(obstacles[2].x + 130, obstacles[2].y);
      this.getCollectible(obstacles[2].x, obstacles[2].y + 100);

      this.getCollectible(obstacles[3].x - 130, obstacles[3].y);
      this.getCollectible(obstacles[3].x, obstacles[3].y - 100);
      this.getCollectible(this.player.x + 280, this.player.y);
      this.getCollectible(obstacles[4].x - 130, obstacles[4].y);
      this.getCollectible(obstacles[4].x, obstacles[4].y + 100);

      this.numSeeds = 10;
      break;

    case 15:
      this.player.x = 400;
      this.player.y = 200;
      var obstacles = this.generateObstacles(0, 0, 6);
      obstacles[0].anims.play('crawl');

      var path1 = new Phaser.Curves.Path(50, 275).lineTo(
        Utils.GlobalSettings.width - 50,
        275
      );

      path1.draw(this.obstaclePathGraphics, 16);

      obstacles[0].setPath(path1);
      obstacles[0].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: true,
        repeat: -1,
        rotateToPath: false,
        verticalAdjust: true
      });

      var path2 = new Phaser.Curves.Path(
        Utils.GlobalSettings.width / 2,
        50
      ).lineTo(Utils.GlobalSettings.width / 2, this.pad.y - 50);

      path2.draw(this.obstaclePathGraphics, 16);

      obstacles[1].setPath(path2);
      obstacles[1].anims.play('crawl');
      obstacles[1].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: true,
        repeat: -1,
        rotateToPath: false,
        verticalAdjust: true
      });

      var path3 = new Phaser.Curves.Path(175, 125).circleTo(75);
      path3.draw(this.obstaclePathGraphics, 16);
      obstacles[2].anims.play('crawl');
      obstacles[2].setPath(path3);
      obstacles[2].startFollow({
        positionOnPath: true,
        duration: 5000,
        yoyo: false,
        repeat: -1,
        rotateToPath: true,
        verticalAdjust: true
      });

      var path4 = new Phaser.Curves.Path(
        Utils.GlobalSettings.width - 25,
        125
      ).circleTo(75);
      path4.draw(this.obstaclePathGraphics, 16);
      obstacles[3].anims.play('crawl');
      obstacles[3].setPath(path4);
      obstacles[3].startFollow({
        positionOnPath: true,
        duration: 4000,
        yoyo: false,
        repeat: -1,
        rotateToPath: true,
        verticalAdjust: true
      });

      var path5 = new Phaser.Curves.Path(
        Utils.GlobalSettings.width - 25,
        this.pad.y - 150
      ).circleTo(75);
      path5.draw(this.obstaclePathGraphics, 16);
      obstacles[4].anims.play('crawl');
      obstacles[4].setPath(path5);
      obstacles[4].startFollow({
        positionOnPath: true,
        duration: 3000,
        yoyo: false,
        repeat: -1,
        rotateToPath: true,
        verticalAdjust: true
      });

      var path6 = new Phaser.Curves.Path(175, this.pad.y - 150).circleTo(75);
      path6.draw(this.obstaclePathGraphics, 16);
      obstacles[5].anims.play('crawl');
      obstacles[5].setPath(path6);
      obstacles[5].startFollow({
        positionOnPath: true,
        duration: 2000,
        yoyo: false,
        repeat: -1,
        rotateToPath: true,
        verticalAdjust: true
      });

      this.getCollectible(100, 125);
      this.getCollectible(100, this.pad.y - 150);
        this.getCollectible(Utils.GlobalSettings.width - 100, this.pad.y - 150);
      this.getCollectible(Utils.GlobalSettings.width - 100, 125);

      this.numSeeds = 4;
      break;
    }
  }

  getCollectible(x, y) {
    var collectible = this.collectibleGroup.getFirstDead();
    if (collectible) {
      collectible.enableBody(true, 0, 0, true, true);
      collectible.x = x;
      collectible.y = y;
      collectible.anims.play('spin');
    }
    return collectible;
  }

  generateObstacles(x, y, amount) {
    var obstacles = [];
    if (amount <= this.obstacleGroup.length) {
      for (var i = 0; i < amount; i++) {
        var obstacle = this.obstacleGroup[i];
        if (obstacle) {
          obstacle.body.setEnable(true);
          obstacle.x = x;
          obstacle.y = y;
          obstacle.visible = true;
          obstacles.push(obstacle);
        }
      }
    }
    return obstacles;
  }

  generateBush(x, y) {
    if (!Utils.SavedSettings.muted) {
      this.collectSnd.play();
    }
    for (var i = 0; i < 10; i++) {
      var bush = this.add.image(
        x + Phaser.Math.RND.integerInRange(-10, 10),
        y + Phaser.Math.RND.integerInRange(-10, 10),
        'atlas1',
        'leaves' + Phaser.Math.RND.integerInRange(1, 4)
      );
      bush.setScale(0);
      bush.setAngle(Phaser.Math.RND.integerInRange(0, 360));
      this.bushGroup.add(bush);
      var scale = 1.5;

      this.tweens.add({
        targets: bush,
        scaleX: scale,
        scaleY: scale,
        ease: 'Back.easeOut',
        duration: 1000,
        delay: 100 * i
      });
    }

    for (var i = 0; i < 10; i++) {
      var flower = this.add.image(
        x + Phaser.Math.RND.integerInRange(-30, 30),
        y + Phaser.Math.RND.integerInRange(-30, 30),
        'atlas1',
        'flower' + Phaser.Math.RND.integerInRange(1, 9)
      );
      flower.setScale(0);
      //flower.setTint(this.hsv[Phaser.Math.RND.integerInRange(0, 360)].color);
      flower.setAngle(Phaser.Math.RND.integerInRange(0, 360));
      this.bushGroup.add(flower);
      var scale = Phaser.Math.RND.integerInRange(1, 1.1);

      this.tweens.add({
        targets: flower,
        scaleX: scale,
        scaleY: scale,
        ease: 'Back.easeOut',
        duration: 1000,
        delay: 100 * i
      });
    }
  }

  movePlayer() {
    if (!this.gameOver && !this.levelCompleted) {
      this.player.anims.play('fly');
      this.curve = new Phaser.Curves.Spline(this.points);

      this.pathConfig = {
        duration: 2000,
        yoyo: false,
        rotateToPath: true,
        rotationOffset: 90,
        verticalAdjust: true,
        onComplete: () => {
          if (!this.gameOver && !this.levelCompleted) {
            this.player.setPath(this.curve);
            this.player.startFollow(this.pathConfig);
          }
        }
      };

      this.player.setPath(this.curve);
      this.player.startFollow(this.pathConfig);
    }
  }

  checkPlayerOffScreen() {
    var offDir = 0;
    if (this.player.x < 0) {
      offDir = 1;
    } else if (this.player.x > Utils.GlobalSettings.width) {
      offDir = 2;
    } else if (this.player.y < 0) {
      offDir = 3;
    } else if (this.player.y > Utils.GlobalSettings.height) {
      offDir = 4;
    }

    if (offDir > 0) {
      if (this.seedsBloomed >= this.numSeeds) {
        if (!this.levelCompleted) {
          this.levelCompleted = true;

          this.level++;
          Utils.SavedSettings.level = this.level;
          Utils.save();

          this.player.stopFollow();
          this.player.body.setEnable(false);
          this.bg2.setVisible(true);
          if (!Utils.mobilecheck() && this.emitter) {
            this.emitter.setVisible(false);
          }

          if (this.level > this.maxLevels) {
            if (!Utils.SavedSettings.muted) {
              this.endSnd.play();
            }
            this.levelTxt.setText('Thanks For Playing!');

            this.restartBtn.y = Utils.GlobalSettings.height / 2;
            this.restartBtn.setVisible(true);
            var restartTween1 = this.tweens.add({
              targets: this.restartBtn,
              alpha: 1,
              ease: 'Linear',
              duration: 1000,
              delay: 1500
            });

            var restartTween2 = this.tweens.add({
              targets: this.restartBtn,
              angle: 360,
              ease: 'Linear',
              duration: 3000,
              loop: -1
            });
          } else {
            this.levelTxt.setText('Level: ' + this.level);
            if (!Utils.SavedSettings.muted) {
              this.winSnd.play();
            }
          }
          this.levelTxt.x =
            Utils.GlobalSettings.width / 2 - this.levelTxt.width / 2;

          this.tweens.add({
            targets: this.screenGroup,
            y: Utils.GlobalSettings.height,
            ease: 'Sine.easeOut',
            duration: 2000,
            onComplete: () => {
              if (this.level <= this.maxLevels) {
                this.cameras.main.fade(
                  1000,
                  Phaser.Math.Between(50, 255),
                  Phaser.Math.Between(50, 255),
                  Phaser.Math.Between(50, 255)
                );
              } else {
                this.level = 1;
                Utils.SavedSettings.level = this.level;
                Utils.save();
              }
            }
          });

          if (this.level <= this.maxLevels) {
            this.tweens.add({
              targets: [this.levelTxt],
              alpha: 0.5,
              ease: 'Sine.easeOut',
              duration: 1000
            });

            this.bushGroup.children.iterate(bush => {
              this.tweens.add({
                targets: bush,
                alpha: 0,
                ease: 'Sine.easeOut',
                duration: 1000
              });
            });
          } else {
            this.player.x = Utils.GlobalSettings.width / 2;
            this.player.y = -Utils.GlobalSettings.height / 2;

            this.tweens.add({
              targets: [this.player],
              alpha: 1,
              scaleX: 5,
              scaleY: 5,
              ease: 'Sine.easeOut',
              duration: 1000,
              delay: 1000
            });
          }

          this.tweens.add({
            targets: [this.player, this.obstaclePathGraphics],
            alpha: 0,
            ease: 'Sine.easeOut',
            duration: 1000
          });

          for (var i = 0; i < this.obstacleGroup.length; i++) {
            this.tweens.add({
              targets: this.obstacleGroup[i],
              alpha: 0,
              ease: 'Sine.easeOut',
              duration: 1000
            });
          }

          this.collectibleGroup.children.iterate(seed => {
            this.tweens.add({
              targets: seed,
              alpha: 0,
              ease: 'Sine.easeOut',
              duration: 1000
            });
          });
        }
      } else {
        this.doGameOver();
      }
    }
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    } else if (key === 'restart') {
      if (data) {
        this.cameras.main.fade(
          1000,
          Phaser.Math.Between(50, 255),
          Phaser.Math.Between(50, 255),
          Phaser.Math.Between(50, 255)
        );
      }
    } else if (key === 'home') {
      if (data) {
        this.scene.start('title');
      }
    }
  }

  update() {
    // if (this.curve) {
    //   this.pathGraphics.clear();
    //   this.pathGraphics.lineStyle(16, 0xffffff, 1);
    //   this.curve.draw(this.pathGraphics, 64);
    // }

    if (this.player && !this.gameOver && !this.levelCompleted) {
      this.checkPlayerOffScreen();
    }

    if (this.drawTimer && this.timerText) {
      var timer = Math.ceil(
        this.maxTime / 1000 - this.drawTimer.getElapsedSeconds()
      );
      this.timerText.setText('' + timer);
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.bg2) {
      this.bg2.destroy();
      this.bg2 = null;
    }

    if (this.levelTxt) {
      this.levelTxt.destroy();
      this.levelTxt = null;
    }

    this.hsv = null;
    this.points = null;

    if (this.pad) {
      this.pad.destroy();
      this.pad = null;
    }

    if (this.graphics) {
      this.graphics.destroy();
      this.graphics = null;
    }

    if (this.particles) {
      this.particles.destroy();
      this.particles = null;
    }

    if (this.emitter) {
      this.emitter = null;
    }

    if (this.player) {
      this.player.destroy();
      this.player = null;
    }

    if (this.playerGroup) {
      this.playerGroup.destroy();
      this.playerGroup = null;
    }

    if (this.bushGroup) {
      this.bushGroup.destroy();
      this.bushGroup = null;
    }

    if (this.collectibleGroup) {
      this.collectibleGroup.destroy();
      this.collectibleGroup = null;
    }

    if (this.obstacleGroup) {
      for (var i = 0; i < this.obstacleGroup.length; i++) {
        if (this.obstacleGroup[i]) {
          this.obstacleGroup[i].destroy();
          this.obstacleGroup[i] = null;
        }
      }
      this.obstacleGroup = null;
    }

    if (this.obstaclePathGraphics) {
      this.obstaclePathGraphics.destroy();
      this.obstaclePathGraphics = null;
    }

    if (this.gameOverTxt) {
      this.gameOverTxt.destroy();
      this.gameOverTxt = null;
    }

    if (this.restartBtn) {
      this.restartBtn.destroy();
      this.restartBtn = null;
    }

    if (this.tutorialImage) {
      this.tutorialImage.destroy();
      this.tutorialImage = null;
    }

    if (this.tutorialHand) {
      this.tutorialHand.destroy();
      this.tutorialHand = null;
    }

    if (this.timerText) {
      this.timerText.destroy();
      this.timerText = null;
    }

    if (this.drawTimer) {
      this.drawTimer.remove();
      this.drawTimer.destroy();
      this.drawTimer = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill main');
  }
}
