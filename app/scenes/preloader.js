import Utils from 'utils/utils';

export default class Preloader extends Phaser.Scene {
  constructor() {
    super('preloader');
    this.progressBar = null;
    this.progressBarRectangle = null;
  }

  preload() {
    this.load.setCORS('Anonymous');

    this.load.image('bg', 'assets/bg.jpg');
    this.load.image('titlebg', 'assets/titlebg.jpg');

    this.load.atlas('atlas1', 'assets/sprites-0.png', 'assets/sprites-0.json');

    this.load.audio('gp', ['assets/audio/gp.ogg', 'assets/audio/gp.m4a']);
    this.load.audio('click', [
      'assets/audio/click.ogg',
      'assets/audio/click.m4a'
    ]);
    this.load.audio('bgm', ['assets/audio/bgm.ogg', 'assets/audio/bgm.m4a']);
    this.load.audio('draw', ['assets/audio/draw.ogg', 'assets/audio/draw.m4a']);
    this.load.audio('collect', [
      'assets/audio/collect.ogg',
      'assets/audio/collect.m4a'
    ]);
    this.load.audio('win', ['assets/audio/win.ogg', 'assets/audio/win.m4a']);
    this.load.audio('dead', ['assets/audio/dead.ogg', 'assets/audio/dead.m4a']);
    this.load.audio('end', ['assets/audio/end.ogg', 'assets/audio/end.m4a']);

    this.load.bitmapFont(
      'modak',
      'assets/fonts/modak_0.png',
      'assets/fonts/modak.xml'
    );

    this.load.on('progress', this.onLoadProgress, this);
    this.load.on('complete', this.onLoadComplete, this);
    this.createProgressBar();

    this.icon = this.add.image(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      'logo'
    );
    this.icon.setOrigin(0.5, 0.5);
  }

  create() {
    Utils.GlobalSettings.bgm = this.sound.add('bgm');

    this.message = this.add.dynamicBitmapText(
      0,
      Utils.GlobalSettings.height - 350,
      'modak',
      'Tap To Play',
      50
    );
    this.message.setOrigin(0, 0);

    this.messageWaveIdx = 0;
    this.messageWave = 0;
    this.messageDelay = 0;
    this.messageColor = [
      0xffa8a8,
      0xffacec,
      0xffa8d3,
      0xfea9f3,
      0xefa9fe,
      0xe7a9fe,
      0xc4abfe
    ];
    this.messageColorOffset = 0;

    this.message.setDisplayCallback(data => {
      data.color = this.messageColor[
        (this.messageColorOffset + this.messageWaveIdx) %
          this.messageColor.length
      ];
      this.messageWaveIdx =
        (this.messageWaveIdx + 1) % this.messageColor.length;
      data.y = Math.cos(this.messageWave + this.messageWaveIdx) * 10;
      this.messageWave += 0.01;

      return data;
    });
    this.message.x = Utils.GlobalSettings.width / 2 - this.message.width / 2;

    this.input.on('pointerdown', () => {
      this.transitionOut();
      //this.scale.toggleFullscreen();
    });

    this.events.on('shutdown', this.shutdown, this);
  }

  transitionOut(target = 'title') {
    var tween = this.tweens.add({
      targets: [this.icon, this.message],
      alpha: 0,
      ease: 'Power1',
      duration: 1000,
      onComplete: () => {
        this.scene.start(target, { from: 'preloader' });
      }
    });
  }

  // extend:

  createProgressBar() {
    var main = this.cameras.main;
    this.progressBarRectangle = new Phaser.Geom.Rectangle(
      0,
      0,
      0.75 * main.width,
      20
    );
    Phaser.Geom.Rectangle.CenterOn(
      this.progressBarRectangle,
      0.5 * main.width,
      main.height - 150
    );
    this.progressBar = this.add.graphics();
  }

  onLoadComplete(loader) {
    //console.log('onLoadComplete', loader);
    this.progressBar.destroy();
  }

  onLoadProgress(progress) {
    var rect = this.progressBarRectangle;
    var color = 0xca59ff;
    this.progressBar
      .clear()
      .fillStyle(0x222222)
      .fillRect(rect.x, rect.y, rect.width, rect.height)
      .fillStyle(color)
      .fillRect(rect.x, rect.y, progress * rect.width, rect.height);
    //console.log('progress', progress);
  }

  update() {
    this.messageWaveIdx = 0;

    if (this.messageDelay++ === 6) {
      this.messageColorOffset =
        (this.messageColorOffset + 1) % this.messageColor.length;
      this.messageDelay = 0;
    }
  }

  shutdown() {
    if (this.icon) {
      this.icon.destroy();
      this.icon = null;
    }

    if (this.progressBarRectangle) {
      this.progressBarRectangle = null;
    }

    this.events.off('shutdown', this.shutdown, this);

    console.log('kill preloader');
  }
}
