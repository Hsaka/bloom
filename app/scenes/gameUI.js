import Utils from 'utils/utils';

export default class GameUI extends Phaser.Scene {
  constructor() {
    super({
      key: 'gameUI',
      active: false
    });
  }

  init(data) {
    this.initData = data;
  }

  create() {
    this.clickSnd = this.sound.add('click');

    this.sprGroup = this.add.group();

    this.soundButton = this.add.sprite(
      Utils.GlobalSettings.width - 50,
      10,
      'atlas1',
      'systembuttons0'
    );
    this.soundButton.setOrigin(0, 0);
    this.soundButton.alpha = 0;
    this.soundButton.setInteractive();

    if (Utils.SavedSettings.muted) {
      this.soundButton.setFrame('systembuttons2');
    }

    this.soundButton.on('pointerup', () => this.soundClick());

    this.soundButton.on('pointerover', event => {
      if (Utils.SavedSettings.muted) {
        this.soundButton.setFrame('systembuttons3');
      } else {
        this.soundButton.setFrame('systembuttons1');
      }
    });

    this.soundButton.on('pointerout', event => {
      if (Utils.SavedSettings.muted) {
        this.soundButton.setFrame('systembuttons2');
      } else {
        this.soundButton.setFrame('systembuttons0');
      }
    });

    this.fullscreenButton = this.add.sprite(
      Utils.GlobalSettings.width - 50,
      60,
      'atlas1',
      'systembuttons4'
    );
    this.fullscreenButton.setOrigin(0, 0);
    this.fullscreenButton.alpha = 0;
    this.fullscreenButton.setInteractive();

    if (this.scale.isFullscreen) {
      this.fullscreenButton.setFrame('systembuttons6');
    }

    this.fullscreenButton.on('pointerup', () => this.fullscreenClick());

    this.fullscreenButton.on('pointerover', event => {
      if (this.scale.isFullscreen) {
        this.fullscreenButton.setFrame('systembuttons7');
      } else {
        this.fullscreenButton.setFrame('systembuttons5');
      }
    });

    this.fullscreenButton.on('pointerout', event => {
      if (this.scale.isFullscreen) {
        this.fullscreenButton.setFrame('systembuttons6');
      } else {
        this.fullscreenButton.setFrame('systembuttons4');
      }
    });

    this.homeButton = this.add.sprite(
      Utils.GlobalSettings.width - 50,
      110,
      'atlas1',
      'systembuttons10'
    );
    this.homeButton.setOrigin(0, 0);
    this.homeButton.alpha = 0;
    this.homeButton.setInteractive();

    this.homeButton.on('pointerup', () => this.homeClick());

    this.homeButton.on('pointerover', event => {
      this.homeButton.setFrame('systembuttons11');
    });

    this.homeButton.on('pointerout', event => {
      this.homeButton.setFrame('systembuttons10');
    });

    this.homeButton.visible = this.initData.showHome;

    this.restartButton = this.add.sprite(
      Utils.GlobalSettings.width - 50,
      160,
      'atlas1',
      'systembuttons8'
    );
    this.restartButton.setOrigin(0, 0);
    this.restartButton.alpha = 0;
    this.restartButton.setInteractive();

    this.restartButton.on('pointerup', () => this.restartClick());

    this.restartButton.on('pointerover', event => {
      this.restartButton.setFrame('systembuttons9');
    });

    this.restartButton.on('pointerout', event => {
      this.restartButton.setFrame('systembuttons8');
    });

    this.restartButton.visible = this.initData.showRestart;

    this.tweens.add({
      targets: this.soundButton,
      alpha: 1,
      ease: 'Linear',
      duration: 1000
    });

    this.tweens.add({
      targets: this.fullscreenButton,
      alpha: 1,
      ease: 'Linear',
      duration: 1000
    });

    this.tweens.add({
      targets: this.restartButton,
      alpha: 1,
      ease: 'Linear',
      duration: 1000
    });

    this.tweens.add({
      targets: this.homeButton,
      alpha: 1,
      ease: 'Linear',
      duration: 1000
    });

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted' && data) {
      Utils.SavedSettings.muted = true;
      Utils.save();
      this.soundButton.setFrame('systembuttons2');
    } else if (key === 'showRestart') {
      if (this.restartButton) {
        this.restartButton.visible = data;
      }
    } else if (key === 'showHome') {
      if (this.homeButton) {
        this.homeButton.visible = data;
      }
    }
  }

  soundClick() {
    Utils.SavedSettings.muted = !Utils.SavedSettings.muted;
    Utils.save();
    if (!Utils.SavedSettings.muted) {
      this.clickSnd.play();
      this.soundButton.setFrame('systembuttons0');
      this.registry.set('muted', false);
      //Utils.BackgroundMusic.bgm1.restart('', 0, 0.5, true);
    } else {
      this.soundButton.setFrame('systembuttons2');
      this.registry.set('muted', true);
      //Utils.BackgroundMusic.bgm1.pause();
    }
  }

  fullscreenClick() {
    if (!Utils.SavedSettings.muted) {
      this.clickSnd.play();
    }

    if (!this.scale.isFullscreen) {
      this.fullscreenButton.setFrame('systembuttons6');
      this.scale.startFullscreen();
    } else {
      this.fullscreenButton.setFrame('systembuttons4');
      this.scale.stopFullscreen();
    }
  }

  restartClick() {
    if (!Utils.SavedSettings.muted) {
      this.clickSnd.play();
    }

    this.registry.set('restart', true);
  }

  homeClick() {
    if (!Utils.SavedSettings.muted) {
      this.clickSnd.play();
    }

    this.registry.set('home', true);
  }

  shutdown() {
    if (this.sprGroup) {
      this.sprGroup.destroy();
      this.sprGroup = null;
    }

    if (this.soundButton) {
      this.soundButton.destroy();
      this.soundButton = null;
    }

    if (this.fullscreenButton) {
      this.fullscreenButton.destroy();
      this.fullscreenButton = null;
    }

    if (this.restartButton) {
      this.restartButton.destroy();
      this.restartButton = null;
    }

    if (this.homeButton) {
      this.homeButton.destroy();
      this.homeButton = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill ui');
  }
}
