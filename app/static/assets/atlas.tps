<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.2</string>
        <key>fileName</key>
        <string>E:/dev/sync/wip/bloom/app/static/assets/atlas.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-hash</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>sprites-{n}.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">raw/arrow.png</key>
            <key type="filename">raw/brush.png</key>
            <key type="filename">raw/flower/flower1.png</key>
            <key type="filename">raw/flower/flower2.png</key>
            <key type="filename">raw/flower/flower3.png</key>
            <key type="filename">raw/flower/flower4.png</key>
            <key type="filename">raw/flower/flower5.png</key>
            <key type="filename">raw/flower/flower6.png</key>
            <key type="filename">raw/flower/flower7.png</key>
            <key type="filename">raw/flower/flower8.png</key>
            <key type="filename">raw/flower/flower9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/bloom/bloom_0.png</key>
            <key type="filename">raw/bloom/bloom_1.png</key>
            <key type="filename">raw/bloom/bloom_10.png</key>
            <key type="filename">raw/bloom/bloom_11.png</key>
            <key type="filename">raw/bloom/bloom_12.png</key>
            <key type="filename">raw/bloom/bloom_13.png</key>
            <key type="filename">raw/bloom/bloom_14.png</key>
            <key type="filename">raw/bloom/bloom_15.png</key>
            <key type="filename">raw/bloom/bloom_16.png</key>
            <key type="filename">raw/bloom/bloom_17.png</key>
            <key type="filename">raw/bloom/bloom_18.png</key>
            <key type="filename">raw/bloom/bloom_19.png</key>
            <key type="filename">raw/bloom/bloom_2.png</key>
            <key type="filename">raw/bloom/bloom_20.png</key>
            <key type="filename">raw/bloom/bloom_3.png</key>
            <key type="filename">raw/bloom/bloom_4.png</key>
            <key type="filename">raw/bloom/bloom_5.png</key>
            <key type="filename">raw/bloom/bloom_6.png</key>
            <key type="filename">raw/bloom/bloom_7.png</key>
            <key type="filename">raw/bloom/bloom_8.png</key>
            <key type="filename">raw/bloom/bloom_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,64,128,128</rect>
                <key>scale9Paddings</key>
                <rect>64,64,128,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/blue.png</key>
            <key type="filename">raw/green.png</key>
            <key type="filename">raw/white.png</key>
            <key type="filename">raw/yellow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/butterfly/butterfly1.png</key>
            <key type="filename">raw/butterfly/butterfly2.png</key>
            <key type="filename">raw/butterfly/butterfly3.png</key>
            <key type="filename">raw/butterfly/butterfly4.png</key>
            <key type="filename">raw/butterfly/butterfly5.png</key>
            <key type="filename">raw/butterfly/butterfly6.png</key>
            <key type="filename">raw/butterfly/butterfly7.png</key>
            <key type="filename">raw/butterfly/butterfly8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,24,23</rect>
                <key>scale9Paddings</key>
                <rect>12,12,24,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/enemies/plant_1.png</key>
            <key type="filename">raw/enemies/plant_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,20,31,41</rect>
                <key>scale9Paddings</key>
                <rect>15,20,31,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/enemies/spider_0.png</key>
            <key type="filename">raw/enemies/spider_1.png</key>
            <key type="filename">raw/enemies/spider_2.png</key>
            <key type="filename">raw/enemies/spider_3.png</key>
            <key type="filename">raw/enemies/spider_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,16,41,32</rect>
                <key>scale9Paddings</key>
                <rect>20,16,41,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/hand.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,14,24,28</rect>
                <key>scale9Paddings</key>
                <rect>12,14,24,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/leaves/leaves1.png</key>
            <key type="filename">raw/leaves/leaves2.png</key>
            <key type="filename">raw/leaves/leaves3.png</key>
            <key type="filename">raw/leaves/leaves4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/pad.png</key>
            <key type="filename">raw/tutorial1.png</key>
            <key type="filename">raw/tutorial2.png</key>
            <key type="filename">raw/tutorial3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,100,320,200</rect>
                <key>scale9Paddings</key>
                <rect>160,100,320,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/play/play0.png</key>
            <key type="filename">raw/play/play1.png</key>
            <key type="filename">raw/restart/restart0.png</key>
            <key type="filename">raw/restart/restart1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,20,40,40</rect>
                <key>scale9Paddings</key>
                <rect>20,20,40,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/seed/seed00.png</key>
            <key type="filename">raw/seed/seed01.png</key>
            <key type="filename">raw/seed/seed010.png</key>
            <key type="filename">raw/seed/seed011.png</key>
            <key type="filename">raw/seed/seed02.png</key>
            <key type="filename">raw/seed/seed03.png</key>
            <key type="filename">raw/seed/seed04.png</key>
            <key type="filename">raw/seed/seed05.png</key>
            <key type="filename">raw/seed/seed06.png</key>
            <key type="filename">raw/seed/seed07.png</key>
            <key type="filename">raw/seed/seed08.png</key>
            <key type="filename">raw/seed/seed09.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,10,10</rect>
                <key>scale9Paddings</key>
                <rect>5,5,10,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/systembuttons/systembuttons0.png</key>
            <key type="filename">raw/systembuttons/systembuttons1.png</key>
            <key type="filename">raw/systembuttons/systembuttons10.png</key>
            <key type="filename">raw/systembuttons/systembuttons11.png</key>
            <key type="filename">raw/systembuttons/systembuttons2.png</key>
            <key type="filename">raw/systembuttons/systembuttons3.png</key>
            <key type="filename">raw/systembuttons/systembuttons4.png</key>
            <key type="filename">raw/systembuttons/systembuttons5.png</key>
            <key type="filename">raw/systembuttons/systembuttons6.png</key>
            <key type="filename">raw/systembuttons/systembuttons7.png</key>
            <key type="filename">raw/systembuttons/systembuttons8.png</key>
            <key type="filename">raw/systembuttons/systembuttons9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,25,25</rect>
                <key>scale9Paddings</key>
                <rect>13,13,25,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>raw/systembuttons</filename>
            <filename>raw/brush.png</filename>
            <filename>raw/arrow.png</filename>
            <filename>raw/leaves</filename>
            <filename>raw/flower</filename>
            <filename>raw/blue.png</filename>
            <filename>raw/green.png</filename>
            <filename>raw/white.png</filename>
            <filename>raw/yellow.png</filename>
            <filename>raw/pad.png</filename>
            <filename>raw/butterfly</filename>
            <filename>raw/seed</filename>
            <filename>raw/enemies</filename>
            <filename>raw/restart</filename>
            <filename>raw/bloom</filename>
            <filename>raw/play</filename>
            <filename>raw/hand.png</filename>
            <filename>raw/tutorial1.png</filename>
            <filename>raw/tutorial2.png</filename>
            <filename>raw/tutorial3.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
